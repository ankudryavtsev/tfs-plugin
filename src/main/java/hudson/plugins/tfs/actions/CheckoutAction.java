package hudson.plugins.tfs.actions;

import hudson.FilePath;
import hudson.plugins.tfs.model.*;
import hudson.plugins.tfs.util.DateUtil;

import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class CheckoutAction
{

    private final String workspaceName;
    private final ProjectData[] projects;
    private final ProjectData rootProjectData;
    private final boolean useUpdate;

    public CheckoutAction(String workspaceName, ProjectData[] projects, boolean useUpdate, ProjectData rootProject)
    {
        this.workspaceName = workspaceName;
        this.projects = projects;
        this.rootProjectData = rootProject;
        this.useUpdate = useUpdate;
    }

    public List<ChangeSet> checkout(Server server, FilePath workspacePath, Calendar lastBuildTimestamp, Calendar currentBuildTimestamp) throws IOException, InterruptedException, ParseException
    {
        Project rootProject = server.getProject(rootProjectData.getProjectPath());
        List<ChangeSet> result = new ArrayList<ChangeSet>();

        Workspaces workspaces = server.getWorkspaces();
        if (workspaces.exists(workspaceName) && !useUpdate)
        {
            Workspace workspace = workspaces.getWorkspace(workspaceName);
            workspaces.deleteWorkspace(workspace);
        }

        Workspace workspace;
        if (!workspaces.exists(workspaceName))
        {
            for (ProjectData project : projects)
            {
                if(!project.isCloaked())
                {
                    FilePath localFolderPath = workspacePath.child(project.getLocalPath());
                    if (!useUpdate && localFolderPath.exists())
                    {
                        localFolderPath.deleteContents();
                    }
                }
            }

            workspace = workspaces.newWorkspace(workspaceName);
            workspace.mapWorkfolder(rootProjectData);
            for (ProjectData projectData : projects)
            {
                workspace.mapWorkfolder(projectData);
            }

        } else
        {
            workspace = workspaces.getWorkspace(workspaceName);
        }

        rootProject.getFiles(rootProjectData.getLocalPath(), "D" + DateUtil.TFS_DATETIME_FORMATTER.get().format(currentBuildTimestamp.getTime()));

        if (lastBuildTimestamp != null)
        {
            result.addAll(rootProject.getDetailedHistory(lastBuildTimestamp, currentBuildTimestamp));
        }

        return result;
    }
}
