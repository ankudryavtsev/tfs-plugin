package hudson.plugins.tfs.model;

import org.kohsuke.stapler.DataBoundConstructor;

import java.io.Serializable;

/**
 * Created by Kudryavtsev_A on 22.12.2014.
 */
public class ProjectData implements Serializable
{
    private final String projectPath;
    private final String localPath;
    private final boolean cloaked;

    @DataBoundConstructor
    public ProjectData(String projectPath, String localPath, boolean cloaked)
    {
        this.projectPath = projectPath;
        this.localPath = localPath == null ? null : localPath.replace('\\', '/');
        this.cloaked = cloaked;
    }

    public String getProjectPath()
    {
        return projectPath;
    }

    public String getLocalPath()
    {
        return localPath;
    }

    public boolean isCloaked()
    {
        return cloaked;
    }

    @Override
    public String toString()
    {
        return "ProjectData{" +
                "projectPath='" + projectPath + '\'' +
                ", localPath='" + localPath + '\'' +
                ", cloaked=" + cloaked +
                '}';
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ProjectData that = (ProjectData) o;

        if (cloaked != that.cloaked) return false;
        if (localPath != null ? !localPath.equals(that.localPath) : that.localPath != null) return false;
        if (!projectPath.equals(that.projectPath)) return false;

        return true;
    }

    @Override
    public int hashCode()
    {
        int result = projectPath.hashCode();
        result = 31 * result + (localPath != null ? localPath.hashCode() : 0);
        result = 31 * result + (cloaked ? 1 : 0);
        return result;
    }
}
