package hudson.plugins.tfs;

import hudson.*;
import hudson.model.*;
import hudson.plugins.tfs.actions.CheckoutAction;
import hudson.plugins.tfs.actions.RemoveWorkspaceAction;
import hudson.plugins.tfs.browsers.TeamFoundationServerRepositoryBrowser;
import hudson.plugins.tfs.commands.PrintCommand;
import hudson.plugins.tfs.model.*;
import hudson.plugins.tfs.model.Project;
import hudson.plugins.tfs.util.BuildVariableResolver;
import hudson.plugins.tfs.util.BuildWorkspaceConfigurationRetriever;
import hudson.plugins.tfs.util.BuildWorkspaceConfigurationRetriever.BuildWorkspaceConfiguration;
import hudson.scm.*;
import hudson.scm.PollingResult.Change;
import hudson.util.FormValidation;
import hudson.util.LogTaskListener;
import hudson.util.Scrambler;
import hudson.util.Secret;
import net.sf.json.JSONObject;
import org.kohsuke.stapler.DataBoundConstructor;
import org.kohsuke.stapler.QueryParameter;
import org.kohsuke.stapler.StaplerRequest;

import java.io.*;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Pattern;

import static hudson.Util.fixEmpty;

/**
 * SCM for Microsoft Team Foundation Server.
 *
 * @author Erik Ramfelt
 */
public class TeamFoundationServerScm extends SCM
{

    public static final String WORKSPACE_ENV_STR = "TFS_WORKSPACE";
    public static final String WORKFOLDER_ENV_STR = "TFS_WORKFOLDER";
    public static final String BUILD_DEFINITION_ENV_STR = "TFS_BUILDDEFINITION";
    public static final String SERVERURL_ENV_STR = "TFS_SERVERURL";
    public static final String USERNAME_ENV_STR = "TFS_USERNAME";
    public static final String WORKSPACE_CHANGESET_ENV_STR = "TFS_CHANGESET";

    private static final String VERSION_SPEC = "VERSION_SPEC";
    private static final Logger logger = Logger.getLogger(TeamFoundationServerScm.class.getName());
    private final String serverUrl;
    private final String rootProjectDir;
    private final String buildDefinitionFile;
    private ProjectData[] projects;
    private ProjectData rootProject;
    private final String workspaceName;
    private final String userName;
    private final String sourceDir;
    private final String fixedSourceDir;
    private final String whatToReplace;
    private final boolean useUpdate;
    private transient
    @Deprecated
    String userPassword;
    private /* almost final */ Secret password;
    private TeamFoundationServerRepositoryBrowser repositoryBrowser;
    private transient String normalizedWorkspaceName;
    private transient String workspaceChangesetVersion;

    @Deprecated
    public TeamFoundationServerScm(String serverUrl, String buildDefinitionFile, String sourceDir, String rootProjectDir, boolean useUpdate, String workspaceName, String userName, String password)
    {
        this(serverUrl, buildDefinitionFile, sourceDir, rootProjectDir, useUpdate, workspaceName, userName, Secret.fromString(password));
    }

    @DataBoundConstructor
    public TeamFoundationServerScm(String serverUrl, String buildDefinitionFile, String sourceDir, String rootProjectDir, boolean useUpdate, String workspaceName, String userName, Secret password)
    {
        this.serverUrl = serverUrl;
        this.buildDefinitionFile = buildDefinitionFile;
        this.useUpdate = useUpdate;
        this.rootProjectDir = (Util.fixEmptyAndTrim(rootProjectDir) == null ? "$/Mob-KISA/dev/src/android" : rootProjectDir);
        this.sourceDir = (Util.fixEmptyAndTrim(sourceDir) == null ? "$(SourceDir):./JEN_TEST" : sourceDir);
        String[] strings = this.sourceDir.split(":");
        this.whatToReplace = strings[0];
        this.fixedSourceDir = (Util.fixEmptyAndTrim(strings[1]) == null ? "${JENKINS_HOME}/KISA_DEV" : strings[1]);
        this.workspaceName = (Util.fixEmptyAndTrim(workspaceName) == null ? "Hudson-${JOB_NAME}-${NODE_NAME}" : workspaceName);
        this.userName = userName;
        this.password = password;
    }

    public String getRootProjectDir()
    {
        return rootProjectDir;
    }

    public String getRootProjectDir(Run<?, ?> run)
    {
        return Util.replaceMacro(rootProjectDir, new BuildVariableResolver(run.getParent()));
    }

    public String getSourceDir()
    {
        return sourceDir;
    }

    public String whatToReplace()
    {
        return whatToReplace;
    }

    public String getSourceDir(Run<?, ?> run)
    {
        return Util.replaceMacro(sourceDir, new BuildVariableResolver(run.getParent()));
    }

    public String getBuildDefinitionFile()
    {
        return buildDefinitionFile;
    }

    public String getBuildDefinitionFile(Run<?, ?> run)
    {
        return substituteBuildParameter(run, buildDefinitionFile);
    }

    private ProjectData[] parseProjectsFromFile(String buildDefinitionFile)
    {
        logger.info("In parseProjectsFromFile");
        File file = new File(buildDefinitionFile);
        logger.info("Abs path: " + file.getAbsolutePath());
        logger.info("File exists: " + file.exists());
        List<ProjectData> list = new ArrayList<ProjectData>();
        try
        {
            BufferedReader in = new BufferedReader(new FileReader(file.getAbsoluteFile()));
            try
            {
                //В цикле построчно считываем файл
                String s;
                while ((s = in.readLine()) != null)
                {
                    s = s.trim();
                    if (!s.isEmpty())
                    {
                        if (s.contains("(cloaked)"))
                        {
                            int dIndex = s.indexOf('$');
                            list.add(new ProjectData(s.substring(dIndex, s.length() - 1), null, true));
                        } else
                        {
                            String[] paths = s.split(": ");
                            if (paths.length == 2)
                            {
                                ProjectData newProject = new ProjectData(paths[0], paths[1].replace(whatToReplace, fixedSourceDir), false);
                                if (newProject.getProjectPath().equalsIgnoreCase(getRootProjectDir()))
                                {
                                    rootProject = newProject;
                                }
                                else
                                    list.add(newProject);
                            }
                            else
                                logger.info("Smth wrong with " + s);
                        }
                    }
                }
            } catch (IOException e)
            {
                e.printStackTrace();
            } finally
            {
                //Также не забываем закрыть файл
                in.close();
            }
        } catch (IOException e)
        {
            e.printStackTrace();
        }
        logger.info(String.format("Parsed: %s projects", list.size()));
        return list.toArray(new ProjectData[list.size()]);
    }

    /* Migrate legacy data */
    private Object readResolve()
    {
        if (password == null && userPassword != null)
            password = Secret.fromString(Scrambler.scramble(userPassword));
        return this;
    }

    // Bean properties need for job configuration
    public String getServerUrl()
    {
        return serverUrl;
    }

    public String getWorkspaceName()
    {
        return workspaceName;
    }

    public boolean isUseUpdate()
    {
        return useUpdate;
    }

    public String getUserPassword()
    {
        return Secret.toString(password);
    }

    public Secret getPassword()
    {
        return password;
    }

    public String getUserName()
    {
        return userName;
    }
    // Bean properties END

    String getWorkspaceName(AbstractBuild<?, ?> build, Computer computer)
    {
        normalizedWorkspaceName = workspaceName;
        if (build != null)
        {
            normalizedWorkspaceName = substituteBuildParameter(build, normalizedWorkspaceName);
            normalizedWorkspaceName = Util.replaceMacro(normalizedWorkspaceName, new BuildVariableResolver(build.getProject(), computer));
        }
        normalizedWorkspaceName = normalizedWorkspaceName.replaceAll("[\"/:<>\\|\\*\\?]+", "_");
        normalizedWorkspaceName = normalizedWorkspaceName.replaceAll("[\\.\\s]+$", "_");
        return normalizedWorkspaceName;
    }

    public String getServerUrl(Run<?, ?> run)
    {
        return substituteBuildParameter(run, serverUrl);
    }

    private String substituteBuildParameter(Run<?, ?> run, String text)
    {
        if (run instanceof AbstractBuild<?, ?>)
        {
            AbstractBuild<?, ?> build = (AbstractBuild<?, ?>) run;
            if (build.getAction(ParametersAction.class) != null)
            {
                return build.getAction(ParametersAction.class).substitute(build, text);
            }
        }
        return text;
    }

    private String downloadTempFile(String serverPath, Server server) throws IOException, InterruptedException
    {
        logger.info("Trying to download: " + serverPath);
        int pos = serverPath.lastIndexOf('/');
        String file = serverPath.substring(pos + 1, serverPath.length());
        File tempDir = Util.createTempDir();
        String resultPath = tempDir.getAbsolutePath() + File.separator + file;
        PrintCommand printCommand = new PrintCommand(server, serverPath, resultPath);
        Reader reader = server.execute(printCommand.getArguments());
        OutputStream outputStream = new FileOutputStream(resultPath);
        int data = reader.read();
        while (data != -1)
        {
            outputStream.write(data);
            data = reader.read();
        }
        reader.close();
        outputStream.close();
        logger.info(String.format("%s downloaded to %s", serverPath, resultPath));
        return resultPath;
    }

    @Override
    public boolean checkout(AbstractBuild<?, ?> build, Launcher launcher, FilePath workspaceFilePath, BuildListener listener, File changelogFile) throws IOException, InterruptedException
    {
        Server server = createServer(new TfTool(getDescriptor().getTfExecutable(), launcher, listener, workspaceFilePath), build);
        try
        {
            projects = parseProjectsFromFile(downloadTempFile(getBuildDefinitionFile(), server));
            WorkspaceConfiguration workspaceConfiguration = new WorkspaceConfiguration(server.getUrl(), workspaceName, projects);
            // Check if the configuration has changed
            if (build.getPreviousBuild() != null)
            {
                BuildWorkspaceConfiguration nodeConfiguration = new BuildWorkspaceConfigurationRetriever().getLatestForNode(build.getBuiltOn(), build.getPreviousBuild());
                if (nodeConfiguration != null)
                {
                    boolean confEquals = workspaceConfiguration.equals(nodeConfiguration);
                    listener.getLogger().println("New configuration == old configuration -> " + confEquals);
                    if (!confEquals)
                    {
                        listener.getLogger().println("New configuration -> " + workspaceConfiguration);
                        listener.getLogger().println("Old configuration -> " + nodeConfiguration);
                    }
                }
                if ((nodeConfiguration != null) &&
                        nodeConfiguration.workspaceExists() && (!workspaceConfiguration.equals(nodeConfiguration)))
                {
                    listener.getLogger().println("Deleting workspace as the configuration has changed since a build was performed on this computer.");
                    new RemoveWorkspaceAction(workspaceConfiguration.getWorkspaceName()).remove(server);
                    nodeConfiguration.setWorkspaceWasRemoved();
                    nodeConfiguration.save();
                }
            }

            build.addAction(workspaceConfiguration);
            CheckoutAction action = new CheckoutAction(workspaceConfiguration.getWorkspaceName(), workspaceConfiguration.getProjects(), isUseUpdate(), rootProject);
            try
            {
                List<ChangeSet> list = action.checkout(server, workspaceFilePath, (build.getPreviousBuild() != null ? build.getPreviousBuild().getTimestamp() : null), build.getTimestamp());
                ChangeSetWriter writer = new ChangeSetWriter();
                writer.write(list, changelogFile);
            } catch (ParseException pe)
            {
                listener.fatalError(pe.getMessage());
                throw new AbortException();
            }

            try
            {
                setWorkspaceChangesetVersion(null);
                logger.info(workspaceConfiguration.toString());
                // TODO: even better would be to call this first, then use the changeset when calling checkout
                int buildChangeset = new Project(server, rootProject.getProjectPath()).getRemoteChangesetVersion(build.getTimestamp());
                setWorkspaceChangesetVersion(Integer.toString(buildChangeset, 10));

                // by adding this action, we prevent calcRevisionsFromBuild() from being called
                build.addAction(new TFSRevisionState(buildChangeset, rootProject.getProjectPath()));
            } catch (ParseException pe)
            {
                listener.fatalError(pe.getMessage());
                throw new AbortException();
            }
        } finally
        {
            server.close();
        }
        return true;
    }

    void setWorkspaceChangesetVersion(String workspaceChangesetVersion)
    {
        this.workspaceChangesetVersion = workspaceChangesetVersion;
    }

    @Override
    public boolean pollChanges(AbstractProject hudsonProject, Launcher launcher, FilePath workspace, TaskListener listener) throws IOException, InterruptedException
    {
        Run<?, ?> lastRun = hudsonProject.getLastBuild();
        if (lastRun == null)
        {
            return true;
        } else
        {
            Server server = createServer(new TfTool(getDescriptor().getTfExecutable(), launcher, listener, workspace), lastRun);
            try
            {
                ProjectData[] projects = getProjects(lastRun);
                for (ProjectData project : projects)
                {
                    if (new Project(server, project.getProjectPath()).getDetailedHistory(lastRun.getTimestamp(), Calendar.getInstance()).size() > 0)
                    {
                        return true;
                    }
                }
                return false;
            } catch (ParseException pe)
            {
                listener.fatalError(pe.getMessage());
                throw new AbortException();
            } finally
            {
                server.close();
            }
        }
    }

    private ProjectData[] getProjects(Run<?, ?> lastRun)
    {
        return projects;
    }

    @Override
    public boolean processWorkspaceBeforeDeletion(AbstractProject<?, ?> project, FilePath workspace, Node node) throws IOException, InterruptedException
    {
        Run<?, ?> lastRun = project.getLastBuild();
        if ((lastRun == null) || !(lastRun instanceof AbstractBuild<?, ?>))
        {
            return true;
        }

        // Due to an error in Hudson core (pre 1.321), null was sent in for all invocations of this method
        // Therefore we try to work around the problem, and see if its only built on one node or not. 
        if (node == null)
        {
            while (lastRun != null)
            {
                AbstractBuild<?, ?> build = (AbstractBuild<?, ?>) lastRun;
                Node buildNode = build.getBuiltOn();
                if (node == null)
                {
                    node = buildNode;
                } else
                {
                    if (!buildNode.getNodeName().equals(node.getNodeName()))
                    {
                        logger.warning("Could not wipe out workspace as there is no way of telling what Node the request is for. Please upgrade Hudson to a newer version.");
                        return false;
                    }
                }
                lastRun = lastRun.getPreviousBuild();
            }
            if (node == null)
            {
                return true;
            }
            lastRun = project.getLastBuild();
        }

        BuildWorkspaceConfiguration configuration = new BuildWorkspaceConfigurationRetriever().getLatestForNode(node, lastRun);
        if ((configuration != null) && configuration.workspaceExists())
        {
            LogTaskListener listener = new LogTaskListener(logger, Level.INFO);
            Launcher launcher = node.createLauncher(listener);
            Server server = createServer(new TfTool(getDescriptor().getTfExecutable(), launcher, listener, workspace), lastRun);
            try
            {
                if (new RemoveWorkspaceAction(configuration.getWorkspaceName()).remove(server))
                {
                    configuration.setWorkspaceWasRemoved();
                    configuration.save();
                }
            } finally
            {
                server.close();
            }
        }
        return true;
    }

    protected Server createServer(TfTool tool, Run<?, ?> run)
    {
        return new Server(tool, getServerUrl(run), getUserName(), getUserPassword());
    }

    @Override
    public boolean requiresWorkspaceForPolling()
    {
        return false;
    }

    @Override
    public boolean supportsPolling()
    {
        return true;
    }

    @Override
    public ChangeLogParser createChangeLogParser()
    {
        return new ChangeSetReader();
    }

    @Override
    public FilePath getModuleRoot(FilePath workspace)
    {
        return workspace.child(rootProject.getLocalPath());
    }

    @Override
    public TeamFoundationServerRepositoryBrowser getBrowser()
    {
        return repositoryBrowser;
    }

    @Override
    public void buildEnvVars(AbstractBuild<?, ?> build, Map<String, String> env)
    {
        super.buildEnvVars(build, env);
        if (normalizedWorkspaceName != null)
        {
            env.put(WORKSPACE_ENV_STR, normalizedWorkspaceName);
        }
        if (env.containsKey("WORKSPACE"))
        {
            if (rootProject != null)
                env.put(WORKFOLDER_ENV_STR, env.get("WORKSPACE") + File.separator + rootProject.getLocalPath());
        }
        if (buildDefinitionFile != null)
        {
            env.put(BUILD_DEFINITION_ENV_STR, buildDefinitionFile);
        }
        if (serverUrl != null)
        {
            env.put(SERVERURL_ENV_STR, serverUrl);
        }
        if (userName != null)
        {
            env.put(USERNAME_ENV_STR, userName);
        }
        if (workspaceChangesetVersion != null && workspaceChangesetVersion.length() > 0)
        {
            env.put(WORKSPACE_CHANGESET_ENV_STR, workspaceChangesetVersion);
        }
    }

    @Override
    public DescriptorImpl getDescriptor()
    {
        return (DescriptorImpl) super.getDescriptor();
    }

    @Override
    public SCMRevisionState calcRevisionsFromBuild(AbstractBuild<?, ?> build, Launcher launcher, TaskListener listener) throws IOException, InterruptedException
    {
        /*
         * This method does nothing, since the work has already been done in
         * the checkout() method, as per the documentation:
         * """
         * As an optimization, SCM implementation can choose to compute SCMRevisionState
         * and add it as an action during check out, in which case this method will not called.
         * """
         */
        return null;
    }

    @Override
    protected PollingResult compareRemoteRevisionWith(AbstractProject<?, ?> project, Launcher launcher, FilePath workspace, TaskListener listener, SCMRevisionState baseline) throws IOException, InterruptedException
    {

        final Launcher localLauncher = launcher != null ? launcher : new Launcher.LocalLauncher(listener);
        if (!(baseline instanceof TFSRevisionState))
        {
            // This plugin was just upgraded, we don't yet have a new-style baseline,
            // so we perform an old-school poll
            boolean shouldBuild = pollChanges(project, localLauncher, workspace, listener);
            return shouldBuild ? PollingResult.BUILD_NOW : PollingResult.NO_CHANGES;
        }
        final TFSRevisionState tfsBaseline = (TFSRevisionState) baseline;
        Run<?, ?> build = project.getLastBuild();
        final TfTool tool = new TfTool(getDescriptor().getTfExecutable(), localLauncher, listener, workspace);
        final Server server = createServer(tool, build);
        final Project tfsProject = new Project(server, rootProject.getProjectPath());
        try
        {
            final List<ChangeSet> briefHistory = tfsProject.getBriefHistory(tfsBaseline.changesetVersion, Calendar.getInstance());

            // TODO: Given we have a tfsBaseline with a changeset,
            // briefHistory will probably always contain at least one entry
            final TFSRevisionState tfsRemote = (briefHistory.size() > 0) ? new TFSRevisionState(briefHistory.get(0).getVersion(), tfsProject.getProjectPath()) : tfsBaseline;

            // TODO: we could return INSIGNIFICANT if all the changesets
            // contain the string "***NO_CI***" at the end of their comment
            final Change change = tfsBaseline.changesetVersion == tfsRemote.changesetVersion ? Change.NONE : Change.SIGNIFICANT;
            return new PollingResult(tfsBaseline, tfsRemote, change);
        } catch (ParseException pe)
        {
            listener.fatalError(pe.getMessage());
            throw new AbortException();
        } finally
        {
            server.close();
        }
    }

    public ProjectData[] getProjects()
    {
        return projects;
    }

    public String getLocalPath()
    {
        return projects[0].getLocalPath();
    }

    @Extension
    public static class DescriptorImpl extends SCMDescriptor<TeamFoundationServerScm>
    {

        public static final Pattern WORKSPACE_NAME_REGEX = Pattern.compile("[^\"/:<>\\|\\*\\?]+[^\\s\\.\"/:<>\\|\\*\\?]$", Pattern.CASE_INSENSITIVE);
        public static final Pattern USER_AT_DOMAIN_REGEX = Pattern.compile("^([^\\/\\\\\"\\[\\]:|<>+=;,\\*@]+)@([a-z][a-z0-9.-]+)$", Pattern.CASE_INSENSITIVE);
        public static final Pattern DOMAIN_SLASH_USER_REGEX = Pattern.compile("^([a-z][a-z0-9.-]+)\\\\([^\\/\\\\\"\\[\\]:|<>+=;,\\*@]+)$", Pattern.CASE_INSENSITIVE);
        private String tfExecutable;

        public DescriptorImpl()
        {
            super(TeamFoundationServerScm.class, TeamFoundationServerRepositoryBrowser.class);
            load();
        }

        public String getTfExecutable()
        {
            if (tfExecutable == null)
            {
                return "tf";
            } else
            {
                return tfExecutable;
            }
        }

        @Override
        public SCM newInstance(StaplerRequest req, JSONObject formData) throws FormException
        {
            TeamFoundationServerScm scm = (TeamFoundationServerScm) super.newInstance(req, formData);
            scm.repositoryBrowser = RepositoryBrowsers.createInstance(TeamFoundationServerRepositoryBrowser.class, req, formData, "browser");
            return scm;
        }

        public FormValidation doExecutableCheck(@QueryParameter final String value)
        {
            return FormValidation.validateExecutable(value);
        }

        private FormValidation doRegexCheck(final Pattern[] regexArray, final String noMatchText, final String nullText, String value)
        {
            value = fixEmpty(value);
            if (value == null)
            {
                if (nullText == null)
                {
                    return FormValidation.ok();
                } else
                {
                    return FormValidation.error(nullText);
                }
            }
            for (Pattern regex : regexArray)
            {
                if (regex.matcher(value).matches())
                {
                    return FormValidation.ok();
                }
            }
            return FormValidation.error(noMatchText);
        }

        public FormValidation doUsernameCheck(@QueryParameter final String value)
        {
            return doRegexCheck(new Pattern[]{DOMAIN_SLASH_USER_REGEX, USER_AT_DOMAIN_REGEX}, "Login name must contain the name of the domain and user", null, value);
        }

        public FormValidation doWorkspaceNameCheck(@QueryParameter final String value)
        {
            return doRegexCheck(new Pattern[]{WORKSPACE_NAME_REGEX}, "Workspace name cannot end with a space or period, and cannot contain any of the following characters: \"/:<>|*?", "Workspace name is mandatory", value);
        }

        @Override
        public boolean configure(StaplerRequest req, JSONObject formData) throws FormException
        {
            tfExecutable = Util.fixEmpty(req.getParameter("tfs.tfExecutable").trim());
            save();
            return true;
        }

        /*
         * This is the name that will show up next to CVS and Subversion when configuring a job
         */
        @Override
        public String getDisplayName()
        {
            return "Team Foundation Server";
        }
    }
}
