package hudson.plugins.tfs.commands;

import hudson.plugins.tfs.util.MaskedArgumentListBuilder;

/**
 * Created by Kudryavtsev_A on 16.01.2015.
 */
public class ResolveCommand extends AbstractCommand
{
    private final String workFolder;

    public ResolveCommand(ServerConfigurationProvider configurationProvider, String workFolder)
    {
        super(configurationProvider);
        this.workFolder = workFolder;
    }

    public MaskedArgumentListBuilder getArguments()
    {
        MaskedArgumentListBuilder arguments = new MaskedArgumentListBuilder();
        arguments.add("resolve");
        arguments.add("-auto:OverwriteLocal");
        arguments.add("-recursive");
        arguments.add(workFolder);
        addLoginArgument(arguments);
        return arguments;
    }
}
