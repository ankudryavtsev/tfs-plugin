package hudson.plugins.tfs.commands;

import hudson.plugins.tfs.model.ProjectData;
import hudson.plugins.tfs.util.MaskedArgumentListBuilder;

public class MapWorkfolderCommand extends AbstractCommand
{

    private final String workspaceName;
    private final ProjectData project;

    public MapWorkfolderCommand(ServerConfigurationProvider provider, ProjectData project)
    {
        this(provider, project, null);
    }

    public MapWorkfolderCommand(ServerConfigurationProvider provider, ProjectData project, String workspaceName)
    {
        super(provider);
        this.project = project;
        this.workspaceName = workspaceName;
    }

    public MaskedArgumentListBuilder getArguments()
    {
        MaskedArgumentListBuilder arguments = new MaskedArgumentListBuilder();
        arguments.add("workfold");
        if (project.isCloaked())
        {
            arguments.add("-cloak");
            arguments.add(project.getProjectPath());
        } else
        {
            arguments.add("-map");
            arguments.add(project.getProjectPath());
            arguments.add(project.getLocalPath());
        }
        if (workspaceName != null)
        {
            arguments.add(String.format("-workspace:%s;%s", workspaceName, getConfig().getUserName()));
        }
        addServerArgument(arguments);
        addLoginArgument(arguments);
        return arguments;
    }
}
