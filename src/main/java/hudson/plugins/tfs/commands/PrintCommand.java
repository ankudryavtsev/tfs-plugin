package hudson.plugins.tfs.commands;

import hudson.plugins.tfs.util.MaskedArgumentListBuilder;

/**
 * Created by Kudryavtsev_A on 22.01.2015.
 */
public class PrintCommand extends AbstractCommand
{
    private final String path, pathToSave;

    public PrintCommand(ServerConfigurationProvider configurationProvider, String path, String pathToSave)
    {
        super(configurationProvider);
        this.path = path;
        this.pathToSave = pathToSave;
    }

    public MaskedArgumentListBuilder getArguments()
    {
        MaskedArgumentListBuilder arguments = new MaskedArgumentListBuilder();
        addServerArgument(arguments);
        addLoginArgument(arguments);
        arguments.add("print");
        arguments.add(path);
        return arguments;
    }
}
